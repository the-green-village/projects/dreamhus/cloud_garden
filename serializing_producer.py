#!/usr/bin/python3
from math import nan, isnan
import requests
import json
import time
import logging
import datetime as dt
import pandas as pd
from tgvfunctions import tgvfunctions
from dataclasses import dataclass
from typing import Callable

# Import API login information
import projectsecrets


logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__file__)


BASE_URL = "https://v4.api.cloudgarden.nl"
SPACES = { # Format: space_id: ["address_number", "room_name", "TUD_room_id", "standardized_name"]
        367: ["28", "Keuken 05.681",                 "TU Delft Room ID: 25.09.00.030", "Kitchen"],
        369: ["28", "Overloop 05.1409",              "TU Delft Room ID: 25.09.01.010", "Corridor First Floor"],
        370: ["28", "Slaapkamer 05.617",             "TU Delft Room ID: 25.09.01.030", "Bedroom 2.4"],
        372: ["28", "Sportkamer 05.2407",            "TU Delft Room ID: 25.09.01.050", "Bedroom 2.5"],
        371: ["28", "Studeerkamer 05.2399",          "TU Delft Room ID: 25.09.01.020", "Bedroom 2.3"],
        368: ["28", "Woonkamer 05.677",              "TU Delft Room ID: 25.09.00.020", "Living Room"],
        373: ["28", "WTW 05.2397",                   "TU Delft Room ID: 25.09.02.010", "Attic"],
        374: ["30", "Woonkamer 05.659",              "TU Delft Room ID: 25.09.00.120", "Living Room"],
        375: ["30", "Keuken 05.662",                 "TU Delft Room ID: 25.09.00.130", "Kitchen"],
        376: ["30", "Overloop 05.668",               "TU Delft Room ID: 25.09.01.110", "Corridor First Floor"],
        377: ["32", "Keuken 05.798",                 "TU Delft Room ID: 25.09.00.230", "Kitchen"],
        382: ["32", "Hal 05.2418",                   "TU Delft Room ID: 25.09.00.210", "Hall"],
        383: ["32", "Badkamer 05.628",               "TU Delft Room ID: 25.09.01.211", "Bathroom"],
        378: ["32", "Woonkamer linksboven 05.2409",  "TU Delft Room ID: 25.09.00.220", "Living Room NE"],
        380: ["32", "Woonkamer linksonder 05.794",   "TU Delft Room ID: 25.09.00.220", "Living Room NW"],
        379: ["32", "Woonkamer rechtsboven 05.1339", "TU Delft Room ID: 25.09.00.220", "Living Room SE"],
        381: ["32", "Woonkamer rechtsonder 05.2410", "TU Delft Room ID: 25.09.00.220", "Living Room SW"],}


@dataclass
class Sensor:
    """Class for storing sensor metadata"""
    display_name: str
    device_id: str
    sensor_id: str
    space_id: int
    online: bool

    def print_info(self):
        print(f"Sensor(\n\t sensor_id: {self.sensor_id}\n\t device_id: {self.device_id}\n\t display_name: {self.display_name}\n\t space_id: {self.space_id})")

def retry(func: Callable) -> Callable:
    """
    Decorator for retrying a function when it raises an exception. The main purpose of this is to elegantly retry when an API request fails.
    By default, the decorator retries 3 times with a delay of 1 second after failing before finally raising an exception

    Exception
    ---------
    `Exception`, raises an exception if the function failed after 3 tries.

    Example
    -------
    ```python
    @retry
    def foo():
        # Will be retried 3 times
        ...
    ```
    """
    def inner(*args, **kwargs):
        retries = 3
        delay = 1
        result = None
        for attempt in range(retries):
            try:
                result = func(*args, **kwargs)
                break
            except:
                if attempt >= retries:
                    raise Exception(f"Function {func.__name__} failed after {retries} attempts")
                time.sleep(delay)
        return result
    return inner


def create_message(sensor: Sensor, measurements) -> dict:
    project_id = "dreamhus"
    keys = ["epochms", "co2", "tvoc", "temp", "hum", "pm0_3", "pm0_5", "pm1", "pm2_5", "pm10", "pmn03", "pmn05", "pmn5", "pmn10", "pmn25"]
    for key in keys: # Account for missing data points
        if key not in measurements:
            measurements[key] = nan

        if key in ["epochms", "co2", "tvoc", "pmn03", "pmn05", "pmn5", "pmn10", "pmn25"] and not isnan(measurements[key]):
            measurements[key] = int(measurements[key])
        else:
            measurements[key] = float(measurements[key])
            
    house_number, room_name, tud_room_id, room_identifier = SPACES[sensor.space_id]
    timestamp = measurements['epochms']
    value = {
        "project_id": project_id,
        "project_description": "",
        "application_id": "Cloud Garden - Air Quality Sensor",
        "application_description": "",
        "device_id": f"Dreamhus {house_number} - {room_identifier}",
        "device_type": "Climate Sensor using PMS7003 PM Sensor and MH-Z19B CO2 Sensor",
        "device_description": f"Sensor ID {sensor.sensor_id} - {sensor.display_name}",
        "device_manufacturer": "Cloud Garden",
        "location_id": f"Dreamhus {house_number} - {room_identifier}",
        "location_description": f"{room_name} - {tud_room_id}",
        "timestamp": timestamp,
        "measurements": [{
                "measurement_id": "CO2",
                "unit": "ppm",
                "value": measurements['co2'],
            }, {
                "measurement_id": "TVOC",
                "value": measurements['tvoc'],
                "unit": "ppb",
            }, {
                "measurement_id": "Temperature",
                "unit": "°C",
                "value": measurements['temp'],
            }, {
                "measurement_id": "Relative Humidity",
                "unit": "%",
                "value": measurements['hum'],
            }, {
                "measurement_id": "PM0.3",
                "unit": "μg/m³",
                "measurement_description": "Calculated Value – Concentration Particulate Matter of 0.3 μm",
                "value": measurements['pm0_3'],
            }, {
                "measurement_id": "PM0.5",
                "unit": "μg/m³",
                "measurement_description": "Calculated Value – Concentration Particulate Matter of 0.5 μm",
                "value": measurements['pm0_5'],
            }, {
                "measurement_id": "PM1",
                "unit": "μg/m³",
                "measurement_description": "Concentration Particulate Matter of 1 μm",
                "value": measurements['pm1'],
            }, {
                "measurement_id": "PM2.5",
                "unit": "μg/m³",
                "measurement_description": "Concentration Particulate Matter of 2.5 μm",
                "value": measurements['pm2_5'],
            }, {
                "measurement_id": "PM10",
                "unit": "μg/m³",
                "measurement_description": "Concentration Particulate Matter of 10 μm",
                "value": measurements['pm10'],
            }, {
                "measurement_id": "PM0.3 # of Particles",
                "unit": "",
                "measurement_description": "Number of Particles >0.3 μm in 0.1 L of Air",
                "value": measurements['pmn03'],
            }, {
                "measurement_id": "PM0.5 # of Particles",
                "unit": "",
                "measurement_description": "Number of Particles >0.5 μm in 0.1 L of Air",
                "value": measurements['pmn05'],
            }, {
                "measurement_id": "PM1 # of Particles",
                "unit": "",
                "measurement_description": "Number of Particles >1 μm in 0.1 L of Air",
                "value": measurements['pmn5'],
            }, {
                "measurement_id": "PM2.5 # of Particles",
                "unit": "",
                "measurement_description": "Number of Particles >2.5 μm in 0.1 L of Air",
                "value": measurements['pmn25'],
            }, {
                "measurement_id": "PM5 # of Particles",
                "unit": "",
                "measurement_description": "Number of Particles >5 μm in 0.1 L of Air",
                "value": measurements['pmn5'],
            }, {
                "measurement_id": "PM10 # of Particles",
                "unit": "",
                "measurement_description": "Number of Particles >10 μm in 0.1 L of Air",
                "value": measurements['pmn10'],
            },]
        }
    return value


def format_time_as_api_string(time: dt.datetime) -> str:
    """
    Function formats a date-time object in the format that the CloudGarden API endpoints expect
    outputted format is MM/DD/YY/hh:mm:ss

    Parameters
    ----------
    `time: dt.datetime`, datetime object representing the desired formatted timestamp

    Return
    ------
    `date_formatted: str`, datetime formatted for the CloudGarden API
    """
    day = str(time.day).zfill(2)
    month = str(time.month).zfill(2)
    year = str(time.year - 2000).zfill(2)
    hours = str(time.hour).zfill(2)
    minutes = str(time.minute).zfill(2)
    seconds = str(time.second).zfill(2)
    milliseconds = str(time.microsecond*1e3).zfill(3)
    date_formatted = f"{month}/{day}/{year}/{hours}:{minutes}:{seconds}:{milliseconds}"

    return date_formatted


@retry
def get_token(username, password) -> str:
    """
    Get a login token from API using username and password

    Parameters
    ----------
    `username: str`, api username
    `password: str`, api password

    Return
    ------
    `token: str`, authentication token for the Cloud Garden API

    Exception
    ---------
    `Exception`, function raises an Exception in the case that the http request results in a non-200 status code
    """
    schema = {
      "username": username,
      "password": password
    }
    res = requests.post(BASE_URL + "/login", json=schema)
    if (res.status_code != 200):
        raise Exception(f"Non 200 status code: {res.status_code}, {res.text}")
    data = json.loads(res.text)
    token = data["token"].replace("JWT ", "") # Get token

    return token


def create_authentication_header(username, password) -> dict:
    """
    Create an authentication header given username and password

    Return
    ------
    `auth_header: dict`, the authentication header as a python dict object
    """
    token = get_token(username, password)
    auth_header = {"Authorization": f"Bearer {token}"}

    return auth_header


@retry
def get_sensors_information_raw(auth_header) -> list:
    """
    Get the raw response from the API for the sensor information

    Parameters
    ----------
    `auth_header: dict`, authentication header containing API token generated by get_token()

    Return
    ------
    `sensors: list[dict]`, list of the raw API response of the sensor information

    Exception
    ---------
    `Exception`, function raises an Exception in the case that the http request results in a non-200 status code
    """
    sensors_information = requests.request("GET", BASE_URL + "/sensors", headers=auth_header)
    if (sensors_information.status_code != 200):
        raise Exception(f"Non 200 status code: {sensors_information.status_code}, {sensors_information.text}")

    sensors_information = json.loads(sensors_information.text)
    return sensors_information


@retry
def get_sensors_information(auth_header) -> list[Sensor]:
    """
    Get the sensor id, device id and display name for all the sensors.

    Parameters
    ----------
    `auth_header: dict`, authentication header containing API token generated by get_token()

    Return
    ------
    `sensors: list[Sensor]`, list of all the connected sensor object

    Exception
    ---------
    `Exception`, function raises an Exception in the case that the http request results in a non-200 status code
    """
    sensors_information = requests.request("GET", BASE_URL + "/sensors", headers=auth_header)
    if (sensors_information.status_code != 200):
        raise Exception(f"Non 200 status code: {sensors_information.status_code}, {sensors_information.text}")

    sensors_information = json.loads(sensors_information.text)
    sensors = []

    for sensor in sensors_information:
        devid = sensor["deviceId"]
        dispname = sensor["displayName"]
        sensorid = sensor["id"] # id entry in the response of /sensors is same as sensorId
        online = sensor["online"]
        space_id = sensor["spaceId"]
        sensors.append(Sensor(dispname, devid, sensorid, space_id, online))

    return sensors


@retry
def get_spaces_information(auth_header: dict) -> list[dict]:
    """
    Get the spaces information for all the spaces

    Parameters
    ----------
    `auth_header: dict`, authentication header containing API token generated by get_token()

    Return
    ------
    `spaces: list[dict]`, list of all the connected sensor object

    Exception
    ---------
    `Exception`, function raises an Exception in the case that the http request results in a non-200 status code
    """
    spaces_information = requests.request("GET", BASE_URL + "/spaces", headers=auth_header)
    if (spaces_information.status_code != 200):
        raise Exception(f"Non 200 status code: {spaces_information.status_code}, {spaces_information.text}")

    spaces_information = json.loads(spaces_information.text)
    return spaces_information


@retry
def get_sensor_measurements(sensor: Sensor, date_from: str, date_to: str, interval: int, auth_header: dict) -> pd.DataFrame|None:
    """
    Get the measurement from the sensor between specified dates, note that the
    API caps the amount of data points to be recieved at 250 per call.

    Parameters
    ----------
    `sensor: Sensor`, sensor object from which to read the data
    `date_from: str`, the start date string formatted as MM/DD/YY/hh:mm:ss
    `date_to: str`, the end date string formatted as MM/DD/YY/hh:mm:ss
    `interval: int`, interval at which to retrieve the data in seconds
    `auth_header: dict`

    Return
    ------
    `df: pd.DataFram`, sensor data formatted as a pandas dataframe

    Exceptions
    ----------
    `Exception`, function raises an Exception in the case that the http request results in a non-200 status code
    """
    res = requests.request("GET",
                       BASE_URL + f"/sensors/{sensor.sensor_id}/measurements?dateFrom={date_from}&dateTo={date_to}&interval={interval}",
                       headers=auth_header)
    if (res.status_code != 200):
        raise Exception(f"Non 200 status code: {res.status_code}, {res.text}")
    data = json.loads(res.text)
    df = pd.json_normalize(data)
    return df

def main():
    topic = projectsecrets.TOPIC 
    username = projectsecrets.CLOUD_GARDEN_API_USERNAME
    password  = projectsecrets.CLOUD_GARDEN_API_PASSWORD

    tgv = tgvfunctions.TGVFunctions(topic)

    # Define start and endtime of request.
    end_time = tgv.round_time(utc=True)
    start_time = end_time - dt.timedelta(minutes=6)

    # Define authentication header
    auth_header = create_authentication_header(username, password)
    logger.info("Created Authenication header")

    sensors = get_sensors_information(auth_header)
    logger.info("Retrieved Sensor info")
    sensors_online = list(filter(lambda x: x.online == True, sensors))

    producer = tgv.make_producer("dreamhus_air_quality_producer");
    for sensor in sensors_online:
        interval = 1
        date_from = format_time_as_api_string(start_time)
        date_to = format_time_as_api_string(end_time)

        data = get_sensor_measurements(sensor, date_from, date_to, interval, auth_header)
        try:
            data = tgv.iso_to_epoch_ms(data, "time")

            logger.info("Retrieved measurements from sensor:")
            logger.debug(sensor)
            for _, row in data.iterrows():
                logger.info("Creating message")
                value = create_message(sensor, row)
                tgv.produce(producer, value)

        except Exception as e:
            logger.error(f"Caught exception: {e}")
            continue


if __name__ == "__main__":
    main()
