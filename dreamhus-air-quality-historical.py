import datetime as dt
from tgvfunctions import tgvfunctions
from serializing_producer import *

# Import API login information
import projectsecrets


def dump_df_to_csv(sensor: Sensor, data: pd.DataFrame) -> None:
    keys = ["eopochms", "time", "co2",
            "tvoc", "temp", "hum",
            "pm0_3", "pm0_5", "pm1",
            "pm2_5", "pm10", "pmn03",
            "pmn05", "pmn5", "pmn10"]
    file_name = "./data/sensor-" + str(sensor.sensor_id) + ".csv"
    for key in keys: # Account for missing data points
        if key not in data:
            data[key] = -1
    with open(file_name, "a") as fb:
        for _, row in data.iterrows():
            fb.write(f"{row['epochms']}, {row['time']}, {row['co2']}, {row['tvoc']}, {row['temp']}, {row['hum']}, {row['pm0_3']}, {row['pm0_5']}, {row['pm1']}, {row['pm2_5']}, {row['pm10']}, {row['pmn03']}, {row['pmn05']}, {row['pmn5']}, {row['pmn10']}\n")

def datetime_to_string(input: dt.datetime) -> str:
    return input.strftime("%d/%m/%Y, %H:%M:%S")

def main():
    topic = projectsecrets.TOPIC
    username = projectsecrets.CLOUD_GARDEN_API_USERNAME
    password  = projectsecrets.CLOUD_GARDEN_API_PASSWORD

    tgv = tgvfunctions.TGVFunctions(topic)
    producer_name = tgv.make_producer("tud_gv_test_cloud_garnde_historic-producer")
    producer = tgv.make_producer(producer_name)

    # Define start and endtime of request.
    start_time = dt.datetime(year=2019, month=8, day=16, hour=2, minute=25, second=0)
    while (start_time < dt.datetime(year=2024, month=6, day=26, hour=11, minute=50, second=0)):
        end_time = start_time + dt.timedelta(minutes=5)
        logger.info(f"Getting data from {datetime_to_string(start_time)} till {datetime_to_string(end_time)}")

        # Define authentication header
        auth_header = create_authentication_header(username, password)
        logger.info("Created Authenication header")

        # Create a list of all sensors
        sensors = get_sensors_information(auth_header)
        logger.info("Retrieved Sensor info")

        # get data per sensor
        timer_start = time.time()
        for s in sensors:
            date_from = format_time_as_api_string(start_time)
            date_to = format_time_as_api_string(end_time)

            try:
                # Retrieve the data
                data = get_sensor_measurements(s, date_from ,date_to, 1, auth_header)
                if data.empty:
                    # logger.info(f"No data for sensor {s.display_name} at time {date_from} - {date_to}")
                    pass
                else:
                    data = tgv.iso_to_epoch_ms(data, "time")

                for _, row in data.iterrows():
                    # logger.info("Creating message")
                    value = create_message(s, row)
                    tgv.produce(producer, value)

            except Exception as e:
                logger.warning(f"Caught exception: {e} when handling data from {date_from} - {date_to}")
                continue
        logger.info(f"Reading data and producing messages took {time.time() - timer_start} s")

        # Update the start time for the next request
        logger.info(f"previous start time: {datetime_to_string(start_time)}")
        logger.info(f"new start time: {datetime_to_string(end_time)}")
        start_time = end_time


if __name__ == "__main__":
    main()
