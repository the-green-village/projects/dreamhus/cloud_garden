# Dreamhus air quality sensors Documentation

- CloudGarden Dashboard: [](https://app.cloudgarden.nl/home)

## Sensors
Custom board with on board sensors:

- temperature/relative humidity
- tvoc sensor
- plantower PMS7003 G7 pm2.5 sensor [datasheet](https://download.kamami.pl/p564008-PMS7003%20series%20data%20manua_English_V2.5.pdf)
- MH-Z19B ndir CO2 sensor [datasheet](https://www.winsen-sensor.com/d/files/infrared-gas-sensor/mh-z19b-co2-manual(ver1_7).pdf)

Data sent over wifi using ESP-12S microcontroller.


- **Notes on the PMS7003**

Sensor sends a string of 32 bytes

- For data 1, 2 and 3, CF is marked. ARe for factory environment use?
  - Data 1 (2 bytes) PM1.0 concentration (ug/m3)
  - Data 2 (2 bytes) PM2.5 concentration (ug/m3)
  - Data 3 (2 bytes) PM10 concentration (ug/m3)
- Data 4, 5 and 6 give the same data but under atmospheric enviromnent
  - Data 4 (2 bytes) PM1.0 concentration (ug/m3)
  - Data 5 (2 bytes) PM2.5 concentration (ug/m3)
  - Data 6 (2 bytes) PM10 concentration (ug/m3)
- Data 7 through 12 count the number of particles
  - Data 7 Number of particles with diameter >0.3 um in 0.1L
  - Data 8 Number of particles with diameter >0.5 um in 0.1L
  - Data 9 Number of particles with diameter >1.0 um in 0.1L
  - Data 10 Number of particles with diameter >2.5 um in 0.1L
  - Data 11 Number of particles with diameter >5.0 um in 0.1L
  - Data 12 Number of particles with diameter >10 um in 0.1L
- Data 13 reserved

Sensor data sample: from the Dreamhus sensors
```
 {'time': '2023-01-01T05:36:24.000Z'
 'basel': 23127.21944035346
 'co2': 430.29896907216494
 'eco2': 418.25036818851254
 'hum': 54.51966126656857
 'p03': 202.76730486008836
 'pm1': 0.27835051546391754
 'pm10': 0.7540500736377025
 'pm2_5': 0.3637702503681885
 'pmn03': 202.76730486008836
 'pmn05': 53.52282768777614
 'pmn1': 3.3328424153166423
 'pmn10': 0
 'pmn25': 0.7422680412371134
 'pmn5': 0.6774668630338734
 'raw': 4523.162002945508
 'temp': 15.434521354933626
 'tvoc': 2.4359351988217965
 'pm0_3': 0.04312732357097991
 'pm0_5': 0.09583087451787356
 'pluginName': ''
 'pluginUid': ''}
```

- `co2` from co2 sensor, measured in `ppm`
- `hum` and `temp` from temperature sesnor
- `pm1`, `pm2_5`, `pm10` particulate matter in ug/m3 from Plantower sensor
- `pmn03`, `pmn05`, `pmn1`, `pmn25`, `pmn5`, `pmn10`, number of particles with diameter greater then shown (pmn = particulate matter number?)
  - Note that `pmn03 > pmn05 > pmn1 > pmn25 > pmn5 > pmn10`
  - The value `p03` is always identical to `pmn03`
- Unaccounted measurments:
  - `pm0_3`, `pm0_5`, calculated 0.3 micron and 0.5 micron concentrations?
  - `raw`, no idea
  - `basel`, no idea
  - `eco2`, equivalent CO2? no idea how this is calculated


## API
To make [API](https://docs.cloudgarden.nl/#/) calls, retrieve a token using `/login`. Token should be passed in the header of any requests:

```python
# Schema for passing login info
schema = {
  "username": username,
  "password": password
}
# Get the token
res = requests.post(base_url + "/login", json=schema)

# Header for token authentication
auth_header = {"Authorization": f"Bearer {token}"}
req = requests.request("GET", base_url + "/{endpoint}", headers=auth_header)
```

- The `v3/login` endpoint returns a token without the `JWT...` prefix, however this token does not work for specific API calls (`/sensors/{sensor_id}/measurements`), use `/login` instead
- The function `/stats/sensors/{device_id}` seems to be broken, it does not return anything and eventually returns a time-out
  - same goes for `/sensors/stats/{device_id}` also results in a timeout

### Measurements
For fetching sensor data use `GET` `/sensors/{sensor_id}/measurements`. Can optionally be formatted as `/sensors/{sensor_id}/measurements?dateFrom={date_from}&dateTo={date_to}&interval={interval}`.

- `dateTo` and `dateFrom` should be passed in the format `MM/DD/YY`
  - time can specified by using the formatting option `MM/DD/YY/hh:mm:ss`
- `interval` given in seconds. Interval (in seconds) used to batch the measurement


Data returned contains a time of measurement field formatted as `YYYY-MM-DDTHH:MM:SS.000Z` with UTC timezone. The following values are measured with (presumed) corresponding units are returned.

| Quantity      | Units | Description |
| :------------ | :---- | :---------  |
| `co2`         | ppm   | CO2 measurement from MH-Z19B |
| `tvoc`        | ppb   | Total volatile organic compounds measured from IC|
| `temp`        | C     | measured from temp+RH sensor|
| `hum`         | %     | Relative humidity measured from temp+RH IC |
| `pm0_3`       | μg/m³ | It seems that this and `pm0_5` are calculated values as they are not directly measured by the sensor |
| `pm0_5`       | μg/m³ | |
| `pm1`         | μg/m³ | Particulate matter 0.3-1 um |
| `pm2_5`       | μg/m³ | Particulate matter 1-2.5 um |
| `pm10`        | μg/m³ | Particulate matter 2.5-10 um |
| `pmn03`       | -     | Number of particles with diameter >0.3um |
| `pmn05`       | -     | Number of particles with diameter >0.5um |
| `pmn1`        | -     | Number of particles with diameter >1.0um |
| `pmn25`       | -     | Number of particles with diameter >2.5um |
| `pmn5`        | -     | Number of particles with diameter >5um |
| `pmn10`       | -     | Number of particles with diameter >10um |

The following measurments are not documented and seem to be inferred/calucated based on sensor data

| Quantity    | Units | Description |
| :---------- | :---- | :---------  |
| p03         | ???   | Same as pmn03? Possibly the sum of all particulate matter > 0.3? |
| eco2        | ???   | Derived quantity not measured directly by CO2 sensor |
| basel       | ???   | ??? |
| raw         | ???   | ??? |
